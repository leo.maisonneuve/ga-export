import pandas as pd
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
KEY_FILE_LOCATION = 'analytics-with-python-386415-64f6aae4683f.json'
VIEW_ID = '179091305'


def initialize_analyticsreporting():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    return analytics


def get_report(analytics):
    return analytics.reports().batchGet(
        body={
            'reportRequests': [
                {
                    'viewId': VIEW_ID,
                    'dateRanges': [{'startDate': '4000daysAgo', 'endDate': 'today'}],
                    'metrics': [{'expression': 'ga:pageviews'}],
                    'dimensions': [{'name': 'ga:pagePath'}],
                    'pageSize': 100000
                }]
        }
    ).execute()


def create_dataframe(response):
    data = {}
    for report in response.get('reports', []):
        column_header = report.get('columnHeader', {})
        dimension_headers = column_header.get('dimensions', [])
        metric_headers = list(
            map(lambda x: x.get("name"), column_header.get('metricHeader', {}).get('metricHeaderEntries', [])))
        rows = report.get('data', {}).get('rows', [])

        for row in rows:
            dimensions = row.get('dimensions', "")[0]
            data[dimensions] = row['metrics'][0]['values'][0]

    return pd.DataFrame(data.items(), columns=dimension_headers + metric_headers)


if __name__ == '__main__':
    analytics = initialize_analyticsreporting()
    response = get_report(analytics)
    df = create_dataframe(response)
    df.to_csv("export.csv")
