import pandas as pd
from google.analytics.data_v1beta import BetaAnalyticsDataClient
from google.analytics.data_v1beta.types import (
    DateRange,
    Dimension,
    Metric,
    RunReportRequest,
)

KEY_FILE_LOCATION = '/Users/leo/Workspace/WYL/GA-export/analytics-with-python-386415-64f6aae4683f.json'
PROPERTY_ID = "292908502"


def print_quota(property_quota, quota_name):
    consumed_quota = property_quota.__getattr__(quota_name).consumed
    remaining_quota = property_quota.__getattr__(quota_name).remaining
    print(f"Used {quota_name}: {remaining_quota / (remaining_quota + consumed_quota)}%")


client = BetaAnalyticsDataClient.from_service_account_json(KEY_FILE_LOCATION)

request = RunReportRequest(
    property=f"properties/{PROPERTY_ID}",
    dimensions=[Dimension(name="pagePath")],
    metrics=[Metric(name="activeUsers"), Metric(name="eventCount")],
    date_ranges=[DateRange(start_date="2020-03-31", end_date="today")],
    limit=100000,
    return_property_quota=True
)
response = client.run_report(request)

data = []
dimension_name = response.dimension_headers[0].name
metrics_name = list(map(lambda x: x.name, response.metric_headers))
for row in response.rows:
    event_name = row.dimension_values[0].value
    metrics_values = list(map(lambda x: x.value, row.metric_values))
    data.append({dimension_name: event_name, **dict(zip(metrics_name, metrics_values))})
df = pd.DataFrame(data)

# %%
print_quota(response.property_quota, quota_name="concurrent_requests")
print_quota(response.property_quota, quota_name="potentially_thresholded_requests_per_hour")
print_quota(response.property_quota, quota_name="server_errors_per_project_per_hour")
print_quota(response.property_quota, quota_name="tokens_per_day")
print_quota(response.property_quota, quota_name="tokens_per_hour")
print_quota(response.property_quota, quota_name="tokens_per_project_per_hour")